
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <link rel="Shortcut Icon" type="image/ico" href="/image/successicon.png">
    <title>Success</title>
</head>
<style type="text/css">
    body{
        background: #f7f9fc;
        font-family: Helvetica, Arial;
    }
    #wrapper
    {
        background: #ffffff;
        -webkit-box-shadow: 0px 16px 46px -22px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 16px 46px -22px rgba(0,0,0,0.75);
        box-shadow: 0px 16px 46px -22px rgba(0,0,0,0.75);

        width: 300px;
        padding-bottom: 10px;

        margin: -170px 0 0 -150px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;

    }
    p{
        text-align: center;
    }
    h2{
        font-weight: normal;
        text-align: center;
        color: #1e6fa0;
    }
    a{
        color: #000;
        text-decoration: none;
        color: #1e6fa0;
    }
    a:hover{
        text-decoration: underline;
    }
</style>
<body>
<div id="wrapper">
    <h2>Your action is done successfully.</h2>
    <p><img src="/image/successicon.png" height="92" /></p>
    <p><a href="/" title="">Home</a></p>
</div>
</body>
</html>
