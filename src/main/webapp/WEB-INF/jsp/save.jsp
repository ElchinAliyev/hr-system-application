<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hr System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/index/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/index/bower_components/bootstrap/dist/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/index/bower_components/bootstrap/dist/css/datepicker3.css">
    <link rel="stylesheet" href="/index/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/index/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="/index/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/index/styles/bootstrap-select.min.css">
    <link rel="stylesheet" href="/index/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/index/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="/index/styles/jquery-ui.css">
    <link rel="stylesheet" href="/index/styles/call.css">
    <link rel="stylesheet" href="/index/styles/select2.min.css">
    <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel=stylesheet>
    <link rel="stylesheet" href="/index/styles/selectize.min.css">
    <link rel="stylesheet" href="/index/styles/bootstrap-select.min.css">
    <script src="/index/scripts/html5shiv.min.js"></script>
    <script src="/index/scripts/respond.min.js"></script>
    <link rel="stylesheet" href="/index/styles/css.css">
    <style>
        .pass_show {
            position: relative
        }

        .pass_show .ptxt {

            position: absolute;

            top: 50%;

            right: 10px;

            z-index: 1;

            color: #f36c01;

            margin-top: -10px;

            cursor: pointer;

            transition: .3s ease all;

        }

        .pass_show .ptxt:hover {
            color: #333333;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">


<div id="msg"></div>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Human Resources</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->


                    <li class="dropdown user user-menu">
                        <div>
                            <a href="/logout"
                               style="color :white;margin-right: 20px;font-size:17px;padding-top: 10px;display: block;">Çıxış </a>
                        </div>

                    </li>

                </ul>
            </div>
        </nav>
    </header>

    <div class="modal  fade" id="modal-success">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-contentId" style="background-color: white">

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Left side column. contains the logo and sidebar -->

    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->


            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard" style="margin-left: 8px"></i> <span>Müraciətlər</span>
                    </a>

                </li>
                <li class="treeview">
                    <button style="background-color: #222d32;border: 0px">
                        <i style="color: #b8c7ce; margin-left: 21px;" class="fa fa-files-o"></i>
                        <a href="/hr/interview" target="_blank"> <span style="color: #b8c7ce;margin-left: 9px">İntervyular</span> </a>
                    </button>
                </li>
                <li class="treeview">
                    <button style="background-color: #222d32;border: 0px" data-toggle="modal"
                            data-target="#modal-default">
                        <i style="color: #b8c7ce; margin-left: 21px;" class="fa fa-files-o"></i>
                        <span style="color: #b8c7ce;margin-left: 9px">Yeni intervyu</span>
                    </button>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <table id="myTable" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>

                <th>Name</th>
                <th>Surname</th>
                <th>Mail</th>
                <th>Phone</th>
                <th>Applied Position</th>
                <th>Applied Date</th>
                <th>View Resume</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${allApplicationForms}" var="applicationForm">
                <tr>
                    <td><c:out value="${applicationForm.name}"/></td>
                    <td><c:out value="${applicationForm.surname}"/></td>
                    <td><c:out value="${applicationForm.mail}"/></td>
                    <td><c:out value="${applicationForm.phone}"/></td>
                    <td><c:out value="${applicationForm.positionName}"/></td>
                    <td><c:out value="${applicationForm.created}"/></td>
                    <td>
                        <a href="/hr/resume/<c:out value="${applicationForm.id}"/>" target="_blank">
                            <button type="button" class="btn btn-primary">Resume</button>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">

        <strong>Copyright &copy; 2020 <a href="№">a2z.az</a></strong> All rights
        reserved.
    </footer>
    <!--check modal-->
    <div class="modal fade" id="check">
        <div class="modal-dialog" style="width:900px;">
            <div class="modal-content" style='padding:15px;max-width: 400px;margin: auto;'>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-center">
                    <i class="fa fa-check-square" style="font-size: 40px; color: green;"></i>
                    <p>İntervyu qeydə alındı</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                            onclick="(function () { window.location.reload()})()">Bağla
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!--check modal-->
    <div class="modal fade" id="success-modal">
        <div class="modal-dialog" style="width:900px;">
            <div class="modal-content" style='padding:15px;max-width: 400px;margin: auto;'>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-center">
                    <i class="fa fa-check-square" style="font-size: 40px; color: green;"></i>
                    <p>Əməliyyat icra edildi</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                            onclick="window.location.reload();">Bağla
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-default">
        <div class="modal-dialog" style="width:900px;">
            <div class="modal-content" style='padding:15px;'>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="text-align:center;">Yeni intervyu</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">

                            <!-- /.form-group -->
                            <div class="form-group">
                                <label for="name">Ad <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Ad">

                            </div>
                            <!-- /.form-group -->
                        </div>

                        <div class="col-md-6">

                            <!-- /.form-group -->
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="surname">Soyad <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="surname" name="surname"
                                           placeholder="Soyad">
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phoneId">Əlaqə nömrəsi <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" id="phoneId" name="phoneId"
                                       placeholder="Əks əlaqə nömrəsi">
                            </div>

                            <!-- /.form-group -->
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="emailAddress">Mail ünvani<span class="text-danger">*</span></label>
                                <input type="email" class="form-control" id="emailAddress" name="emailAddress"
                                       placeholder="Mail ünvani">
                            </div>

                            <!-- /.form-group -->
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="intervyuDate">Intervyu tarixi<span class="text-danger">*</span></label>
                                <input type="date" class="form-control" id="intervyuDate" name="intervyuDate"
                                       placeholder="Intervyunun tarixi">
                            </div>

                            <!-- /.form-group -->
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Bağla</button>
                        <!-- <button type="button" id="search" class="btn btn-primary">Axtar</button>-->
                        <button type="button" id="save" class="btn btn-primary">Yadda saxla</button>


                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


    </div>

    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <label>Current Password</label>
                    <div class="form-group pass_show">
                        <input id="currentPass" type="password" class="form-control" placeholder="Current Password">
                    </div>
                    <label>New Password</label>
                    <div class="form-group pass_show">
                        <input id="newPass1" type="password" class="form-control" placeholder="New Password">
                    </div>
                    <label>Confirm Password</label>
                    <div class="form-group pass_show">
                        <input id="newPass2" type="password" class="form-control" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="submitPass" class="btn btn-default" onclick="checkPass()">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="modal2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Müraciətin məzmunu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="content-info-watch-content">Modal body text goes here.</p><br>
                </div>
                <div class="modal-body modal-text" style="border-top: 1px solid #f4f4f4">
                    <button class="btn btn-success">Şərh əlavə et</button>
                    <button class="btn btn-primary">Şərhi gör</button>
                    <div class="text"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="content-info-watch-status">...</button>
                </div>
            </div>
        </div>
    </div>
    <!-- ./wrapper -->
</div>

<!-- jQuery 3 -->
<script src="/index/bower_components/jquery/dist/jquery.min.js"></script>
<!--    <script src="/index/scripts/jquery-1.9.1.js"></script>/-->
<script src="/index/scripts/jquery-ui.js"></script>
<script src="/index/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/index/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/index/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!--    <script src="/index/scripts/bootstrap1.min.js"></script>-->
<script src="/index/bower_components/bootstrap/dist/js/moment-with-locales.min.js"></script>
<script src="/index/bower_components/bootstrap/dist/js/moment.js"></script>
<script src="/index/bower_components/bootstrap/dist/js/bootstrap-datetimepicker.min.js"></script>
<script src="/index/bower_components/bootstrap/dist/js/bootstrap-datetimepicker.js"></script>
<script src="/index/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/index/bower_components/fastclick/lib/fastclick.js"></script>
<script src="/index/dist/js/adminlte.min.js"></script>
<script src="/index/dist/js/demo.js"></script>
<script src="/index/scripts/call.js"></script>

<script src="/index/scripts/moment.min.js"></script>
<script src="/index/scripts/select2.min.js"></script>
<script src="/index/scripts/selectize.min.js"></script>

<!-- page script -->
<script>
    /* 0 - sondurmek 1 - baslatmaq */
    var procedure_type = 'close';
    var procedure_item_id = 0;


    function openModal2() {
        $('#modal2').modal('show');
        $('#content-info-watch-status').show();
        let e = event || window.event;
        let statusName;
        procedure_item_id = e.target.getAttribute('data-id');
        if (e.target.getAttribute('data-statusName') == 'closed') {
            statusName = 'Müraciəti Yenidən Başlat';
            procedure_type = 'open';
        } else if (e.target.getAttribute('data-statusName') == 'resolved') {
            statusName = 'Müraciəti Bitir';
            procedure_type = 'close';
        } else {
            $('#content-info-watch-status').hide();
        }
        $('#content-info-watch-content').text(e.target.getAttribute('data-content'));
        $('#content-info-watch-status').text(statusName);
    }


    $('#content-info-watch-status').click(function (e) {
        $.ajax({
            url: '/operator/query/' + procedure_item_id + '/' + procedure_type,
            type: 'POST',
            data: {},
            success: function (data) {
                $('#modal2').modal('hide');
                $('#success-modal').modal();
            }
        });
    });


    $(document).ready(function () {

        $('#save').click(function () {
            var name = $('#name').val();
            var surname = $('#surname').val();
            var emailAddress = $('#emailAddress').val();
            var phone = $('#phoneId').val();
            var intervyuDate = $('#intervyuDate').val();


            var data = {
                name: name,
                surname: surname,
                phone: phone,
                mail: emailAddress,
                intervyuDate: intervyuDate
            };
            if (name.length > 0 && surname.length > 0 && emailAddress.length > 0 && phone.length > 0 && intervyuDate.length > 0) {
                $.ajax({
                    url: '/hr/interview',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        if (data == true) {
                            $('#modal-default').modal('hide');
                            $('#check').modal('show');

                        } else {
                            alert("Xəta baş verdi")
                        }
                    }
                })

                $('#name').val('');
                $('#surname').val('');
                $('#emailAddress').val('');
                $('#phoneId').val('');
                $('#intervyuDate').val('');
            }


        });
    });

    var checkPass = function () {

        var currentPass = $('#currentPass').val();
        var newPass1 = $('#newPass1').val();
        var newPass2 = $('#newPass2').val();

        var data = {
            currentPass: currentPass,
            newPass1: newPass1,
            newPass2: newPass2
        };

        if (currentPass.length > 0) {
            if (newPass1.length > 7) {
                if (newPass1 == newPass2) {
                    $.ajax({
                        url: '/user/change/password',
                        type: 'POST',
                        data: data,
                        success: function (data) {
                            if (data.success) {
                                $('#success-modal').modal('show');
                                $('#myModal2').modal('hide');
                            } else {
                                alert(data.message)
                            }
                        }
                    })

                } else {
                    alert("Yazdiginiz parollar ferqlidir")
                }
            } else {
                alert("Parol minimum 8 simvol olmalidir")
            }
        } else {
            alert("Hal hazirki parolu qeyd edin")
        }
    };

    $(document).ready(function () {
        $('.pass_show').append('<span class="ptxt">Show</span>');
    });


    $(document).on('click', '.pass_show .ptxt', function () {

        $(this).text($(this).text() == "Show" ? "Hide" : "Show");

        $(this).prev().attr('type', function (index, attr) {
            return attr == 'password' ? 'text' : 'password';
        });

    });
</script>

<script>
    $(document).ready(function () {


        var table = $('#myTable').DataTable({});
    });
</script>


</body>
</html>
