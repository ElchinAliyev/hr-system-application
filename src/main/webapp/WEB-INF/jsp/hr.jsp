<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
    <meta name="author" content="AdminKit">
    <meta name="keywords"
          content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">

    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link rel="shortcut icon" href="/hr/img/icons/icon-48x48.png"/>

    <link rel="canonical" href="index-2.html"/>

    <title>HRM System</title>

    <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel=stylesheet>
    <link href="/hr/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&amp;display=swap" rel="stylesheet">

    <!-- BEGIN SETTINGS -->
    <script src="/hr/js/settings.js"></script>
    <!-- END SETTINGS -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120946860-10"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-120946860-10', {'anonymize_ip': true});
    </script>
</head>
<!--
  HOW TO USE:
  data-theme: default (default), dark, light
  data-layout: fluid (default), boxed
  data-sidebar: left (default), right
-->

<body data-theme="default" data-layout="fluid" data-sidebar="left">
<div class="wrapper">
    <nav id="sidebar" class="sidebar">
        <div class="sidebar-content js-simplebar">
            <a class="sidebar-brand" href="index.html">
                <span class="align-middle">Human Resource Management Page</span>
            </a>

            <ul class="sidebar-nav">
                <li class="sidebar-header">
                    Pages
                </li>
                <li class="sidebar-item active">
                    <a data-bs-target="#dashboards" data-bs-toggle="collapse" class="sidebar-link">
                        <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Staff</span>
                    </a>
                    <ul id="dashboards" class="sidebar-dropdown list-unstyled collapse show" data-bs-parent="#sidebar">
                        <%--                        <li class="sidebar-item active"><a class="sidebar-link" href="index.html">Analytics</a></li>--%>
                        <li class="sidebar-item"><a class="sidebar-link" href="#employeesTable">Employees<span
                                class="sidebar-badge badge bg-primary"></span></a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="#candidatesTable">Candidates<span
                                class="sidebar-badge badge bg-primary"></span></a></li>
                        <%--                        <li class="sidebar-item"><a class="sidebar-link" href="#interviewsTable">Interviews<span--%>
                        <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                        <%--                        <li class="sidebar-item" ><a class="sidebar-link">New Interview<span--%>
                        <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a data-bs-target="#pages" data-bs-toggle="collapse" class="sidebar-link collapsed">
                        <i class="align-middle" data-feather="layout"></i> <span class="align-middle">HR</span>
                    </a>
                    <ul id="pages" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">
                        <li class="sidebar-item"><a class="sidebar-link" href="#interviewsTable">Interviews<span
                                class="sidebar-badge badge bg-primary"></span></a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="#newIntervyu">Invite Candidate</a></li>
<%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-clients.html">Policies<span--%>
<%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
<%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-pricing.html">Reports<span--%>
<%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                        <%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-chat.html">Chat <span--%>
                        <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                        <%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-blank.html">Blank Page</a></li>--%>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a data-bs-target="#performancePages" data-bs-toggle="collapse" class="sidebar-link collapsed">
                        <i class="align-middle" data-feather="layout"></i> <span class="align-middle">Performance</span>
                    </a>
                    <ul id="performancePages" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">
                        <li class="sidebar-item"><a class="sidebar-link" href="#employeePerformance">Employee Performance<span
                                class="sidebar-badge badge bg-primary"></span></a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="#suggestsComplaints">Suggests/Complaints</a></li>
                    </ul>
                </li>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a class="sidebar-link" href="pages-tasks.html">--%>
                <%--                        <i class="align-middle" data-feather="list"></i> <span class="align-middle">Tasks</span>--%>
                <%--                        <span class="sidebar-badge badge bg-primary"></span>--%>
                <%--                    </a>--%>
                <%--                </li>--%>

                <%--                <li class="sidebar-item">--%>
                <%--                    <a class="sidebar-link" href="calendar.html">--%>
                <%--                        <i class="align-middle" data-feather="calendar"></i> <span class="align-middle">Calendar</span>--%>
                <%--                        <span class="sidebar-badge badge bg-primary"></span>--%>
                <%--                    </a>--%>
                <%--                </li>--%>

                <%--                <li class="sidebar-item">--%>
                <%--                    <a href="#auth" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="users"></i> <span class="align-middle">Auth</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="auth" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-sign-in.html">Sign In</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-sign-up.html">Sign Up</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-reset-password.html">Reset Password <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-404.html">404 Page <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="pages-500.html">500 Page <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>

                <%--                <li class="sidebar-header">--%>
                <%--                    Tools & Components--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a data-bs-target="#ui" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="briefcase"></i> <span class="align-middle">UI Elements</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="ui" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="ui-alerts.html">Alerts</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="ui-buttons.html">Buttons</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="ui-cards.html">Cards</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="ui-general.html">General</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="ui-grid.html">Grid</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="ui-modals.html">Modals</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="ui-tabs.html">Tabs <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="ui-typography.html">Typography</a></li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a data-bs-target="#icons" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="coffee"></i> <span class="align-middle">Icons</span>--%>
                <%--                        <span class="sidebar-badge badge bg-light">1.500+</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="icons" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="icons-feather.html">Feather</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="icons-font-awesome.html">Font Awesome <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a data-bs-target="#forms" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="check-circle"></i> <span class="align-middle">Forms</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="forms" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="forms-basic-inputs.html">Basic Inputs</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="forms-layouts.html">Form Layouts <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="forms-input-groups.html">Input Groups <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a class="sidebar-link" href="tables-bootstrap.html">--%>
                <%--                        <i class="align-middle" data-feather="list"></i> <span class="align-middle">Tables</span>--%>
                <%--                    </a>--%>
                <%--                </li>--%>

                <%--                <li class="sidebar-header">--%>
                <%--                    Plugins & Addons--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a data-bs-target="#form-plugins" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="check-square"></i> <span class="align-middle">Form Plugins</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="form-plugins" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="forms-advanced-inputs.html">Advanced Inputs <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="forms-editors.html">Editors <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="forms-validation.html">Validation <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a data-bs-target="#datatables" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="list"></i> <span class="align-middle">DataTables</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="datatables" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="tables-datatables-responsive.html">Responsive Table <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="tables-datatables-buttons.html">Table with Buttons <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="tables-datatables-column-search.html">Column Search <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="tables-datatables-ajax.html">Ajax Sourced Data <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a data-bs-target="#charts" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="bar-chart-2"></i> <span class="align-middle">Charts</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="charts" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="charts-chartjs.html">Chart.js</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="charts-apexcharts.html">ApexCharts <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a class="sidebar-link" href="notifications.html">--%>
                <%--                        <i class="align-middle" data-feather="bell"></i> <span class="align-middle">Notifications</span>--%>
                <%--                        <span class="sidebar-badge badge bg-primary"></span>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                <li class="sidebar-item">--%>
                <%--                    <a data-bs-target="#maps" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="map"></i> <span class="align-middle">Maps</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="maps" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="maps-google.html">Google Maps</a></li>--%>
                <%--                        <li class="sidebar-item"><a class="sidebar-link" href="maps-vector.html">Vector Maps <span--%>
                <%--                                class="sidebar-badge badge bg-primary"></span></a></li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>

                <%--                <li class="sidebar-item">--%>
                <%--                    <a data-bs-target="#multi" data-bs-toggle="collapse" class="sidebar-link collapsed">--%>
                <%--                        <i class="align-middle" data-feather="corner-right-down"></i> <span class="align-middle">Multi Level</span>--%>
                <%--                    </a>--%>
                <%--                    <ul id="multi" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar">--%>
                <%--                        <li class="sidebar-item">--%>
                <%--                            <a data-bs-target="#multi-2" data-bs-toggle="collapse" class="sidebar-link collapsed">Two Levels</a>--%>
                <%--                            <ul id="multi-2" class="sidebar-dropdown list-unstyled collapse">--%>
                <%--                                <li class="sidebar-item">--%>
                <%--                                    <a class="sidebar-link" href="#">Item 1</a>--%>
                <%--                                    <a class="sidebar-link" href="#">Item 2</a>--%>
                <%--                                </li>--%>
                <%--                            </ul>--%>
                <%--                        </li>--%>
                <%--                        <li class="sidebar-item">--%>
                <%--                            <a data-bs-target="#multi-3" data-bs-toggle="collapse" class="sidebar-link collapsed">Three Levels</a>--%>
                <%--                            <ul id="multi-3" class="sidebar-dropdown list-unstyled collapse">--%>
                <%--                                <li class="sidebar-item">--%>
                <%--                                    <a data-bs-target="#multi-3-1" data-bs-toggle="collapse" class="sidebar-link collapsed">Item 1</a>--%>
                <%--                                    <ul id="multi-3-1" class="sidebar-dropdown list-unstyled collapse">--%>
                <%--                                        <li class="sidebar-item">--%>
                <%--                                            <a class="sidebar-link" href="#">Item 1</a>--%>
                <%--                                            <a class="sidebar-link" href="#">Item 2</a>--%>
                <%--                                        </li>--%>
                <%--                                    </ul>--%>
                <%--                                </li>--%>
                <%--                                <li class="sidebar-item">--%>
                <%--                                    <a class="sidebar-link" href="#">Item 2</a>--%>
                <%--                                </li>--%>
                <%--                            </ul>--%>
                <%--                        </li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>
                <%--            </ul>--%>
    </nav>

    <div class="main">
        <nav class="navbar navbar-expand navbar-light navbar-bg">
            <a class="sidebar-toggle d-flex">
                <i class="hamburger align-self-center"></i>
            </a>

<%--            <form class="d-none d-sm-inline-block">--%>
<%--                <div class="input-group input-group-navbar">--%>
<%--                    <input type="text" class="form-control" placeholder="Search…" aria-label="Search">--%>
<%--                    <button class="btn" type="button">--%>
<%--                        <i class="align-middle" data-feather="search"></i>--%>
<%--                    </button>--%>
<%--                </div>--%>
<%--            </form>--%>

            <div class="navbar-collapse collapse">
                <ul class="navbar-nav navbar-align">
<%--                    <li class="nav-item dropdown">--%>
<%--                        <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">--%>
<%--                            <div class="position-relative">--%>
<%--                                <i class="align-middle" data-feather="bell"></i>--%>
<%--                                <span class="indicator">4</span>--%>
<%--                            </div>--%>
<%--                        </a>--%>
<%--                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0"--%>
<%--                             aria-labelledby="alertsDropdown">--%>
<%--                            <div class="dropdown-menu-header">--%>
<%--                                4 New Notifications--%>
<%--                            </div>--%>
<%--                            <div class="list-group">--%>
<%--                                <a href="#" class="list-group-item">--%>
<%--                                    <div class="row g-0 align-items-center">--%>
<%--                                        <div class="col-2">--%>
<%--                                            <i class="text-danger" data-feather="alert-circle"></i>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-10">--%>
<%--                                            <div class="text-dark">Update completed</div>--%>
<%--                                            <div class="text-muted small mt-1">Restart server 12 to complete the--%>
<%--                                                update.--%>
<%--                                            </div>--%>
<%--                                            <div class="text-muted small mt-1">30m ago</div>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </a>--%>
<%--                                <a href="#" class="list-group-item">--%>
<%--                                    <div class="row g-0 align-items-center">--%>
<%--                                        <div class="col-2">--%>
<%--                                            <i class="text-warning" data-feather="bell"></i>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-10">--%>
<%--                                            <div class="text-dark">Lorem ipsum</div>--%>
<%--                                            <div class="text-muted small mt-1">Aliquam ex eros, imperdiet vulputate--%>
<%--                                                hendrerit et.--%>
<%--                                            </div>--%>
<%--                                            <div class="text-muted small mt-1">2h ago</div>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </a>--%>
<%--                                <a href="#" class="list-group-item">--%>
<%--                                    <div class="row g-0 align-items-center">--%>
<%--                                        <div class="col-2">--%>
<%--                                            <i class="text-primary" data-feather="home"></i>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-10">--%>
<%--                                            <div class="text-dark">Login from 192.186.1.8</div>--%>
<%--                                            <div class="text-muted small mt-1">5h ago</div>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </a>--%>
<%--                                <a href="#" class="list-group-item">--%>
<%--                                    <div class="row g-0 align-items-center">--%>
<%--                                        <div class="col-2">--%>
<%--                                            <i class="text-success" data-feather="user-plus"></i>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-10">--%>
<%--                                            <div class="text-dark">New connection</div>--%>
<%--                                            <div class="text-muted small mt-1">Christina accepted your request.</div>--%>
<%--                                            <div class="text-muted small mt-1">14h ago</div>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </a>--%>
<%--                            </div>--%>
<%--                            <div class="dropdown-menu-footer">--%>
<%--                                <a href="#" class="text-muted">Show all notifications</a>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </li>--%>
<%--                    <li class="nav-item dropdown">--%>
<%--                        <a class="nav-icon dropdown-toggle" href="#" id="messagesDropdown" data-bs-toggle="dropdown">--%>
<%--                            <div class="position-relative">--%>
<%--                                <i class="align-middle" data-feather="message-square"></i>--%>
<%--                            </div>--%>
<%--                        </a>--%>
<%--                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0"--%>
<%--                             aria-labelledby="messagesDropdown">--%>
<%--                            <div class="dropdown-menu-header">--%>
<%--                                <div class="position-relative">--%>
<%--                                    4 New Messages--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="list-group">--%>
<%--                                <a href="#" class="list-group-item">--%>
<%--                                    <div class="row g-0 align-items-center">--%>
<%--                                        <div class="col-2">--%>
<%--                                            <img src="/hr/img/avatars/avatar-5.jpg"--%>
<%--                                                 class="avatar img-fluid rounded-circle" alt="Vanessa Tucker">--%>
<%--                                        </div>--%>
<%--                                        <div class="col-10 ps-2">--%>
<%--                                            <div class="text-dark">Vanessa Tucker</div>--%>
<%--                                            <div class="text-muted small mt-1">Nam pretium turpis et arcu. Duis arcu--%>
<%--                                                tortor.--%>
<%--                                            </div>--%>
<%--                                            <div class="text-muted small mt-1">15m ago</div>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </a>--%>
<%--                                <a href="#" class="list-group-item">--%>
<%--                                    <div class="row g-0 align-items-center">--%>
<%--                                        <div class="col-2">--%>
<%--                                            <img src="/hr/img/avatars/avatar-2.jpg"--%>
<%--                                                 class="avatar img-fluid rounded-circle" alt="William Harris">--%>
<%--                                        </div>--%>
<%--                                        <div class="col-10 ps-2">--%>
<%--                                            <div class="text-dark">William Harris</div>--%>
<%--                                            <div class="text-muted small mt-1">Curabitur ligula sapien euismod vitae.--%>
<%--                                            </div>--%>
<%--                                            <div class="text-muted small mt-1">2h ago</div>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </a>--%>
<%--                                <a href="#" class="list-group-item">--%>
<%--                                    <div class="row g-0 align-items-center">--%>
<%--                                        <div class="col-2">--%>
<%--                                            <img src="/hr/img/avatars/avatar-4.jpg"--%>
<%--                                                 class="avatar img-fluid rounded-circle" alt="Christina Mason">--%>
<%--                                        </div>--%>
<%--                                        <div class="col-10 ps-2">--%>
<%--                                            <div class="text-dark">Christina Mason</div>--%>
<%--                                            <div class="text-muted small mt-1">Pellentesque auctor neque nec urna.</div>--%>
<%--                                            <div class="text-muted small mt-1">4h ago</div>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </a>--%>
<%--                                <a href="#" class="list-group-item">--%>
<%--                                    <div class="row g-0 align-items-center">--%>
<%--                                        <div class="col-2">--%>
<%--                                            <img src="/hr/img/avatars/avatar-3.jpg"--%>
<%--                                                 class="avatar img-fluid rounded-circle" alt="Sharon Lessman">--%>
<%--                                        </div>--%>
<%--                                        <div class="col-10 ps-2">--%>
<%--                                            <div class="text-dark">Sharon Lessman</div>--%>
<%--                                            <div class="text-muted small mt-1">Aenean tellus metus, bibendum sed,--%>
<%--                                                posuere ac, mattis non.--%>
<%--                                            </div>--%>
<%--                                            <div class="text-muted small mt-1">5h ago</div>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </a>--%>
<%--                            </div>--%>
<%--                            <div class="dropdown-menu-footer">--%>
<%--                                <a href="#" class="text-muted">Show all messages</a>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </li>--%>
<%--                    <li class="nav-item dropdown">--%>
<%--                        <a class="nav-flag dropdown-toggle" href="#" id="languageDropdown" data-bs-toggle="dropdown">--%>
<%--                            <img src="/hr/img/flags/us.png" alt="English"/>--%>
<%--                        </a>--%>
<%--                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="languageDropdown">--%>
<%--                            <a class="dropdown-item" href="#">--%>
<%--                                <img src="/hr/img/flags/us.png" alt="English" width="20" class="align-middle me-1"/>--%>
<%--                                <span class="align-middle">English</span>--%>
<%--                            </a>--%>
<%--                            <a class="dropdown-item" href="#">--%>
<%--                                <img src="/hr/img/flags/es.png" alt="Spanish" width="20" class="align-middle me-1"/>--%>
<%--                                <span class="align-middle">Spanish</span>--%>
<%--                            </a>--%>
<%--                            <a class="dropdown-item" href="#">--%>
<%--                                <img src="/hr/img/flags/ru.png" alt="Russian" width="20" class="align-middle me-1"/>--%>
<%--                                <span class="align-middle">Russian</span>--%>
<%--                            </a>--%>
<%--                            <a class="dropdown-item" href="#">--%>
<%--                                <img src="/hr/img/flags/de.png" alt="German" width="20" class="align-middle me-1"/>--%>
<%--                                <span class="align-middle">German</span>--%>
<%--                            </a>--%>
<%--                        </div>--%>
<%--                    </li>--%>
                    <li class="nav-item dropdown">
                        <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
                            <i class="align-middle" data-feather="settings"></i>
                        </a>

                        <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-bs-toggle="dropdown">
                            <img src="/hr/img/avatars/avatar.jpg" class="avatar img-fluid rounded me-1"
                                 alt="Charles Hall"/> <span class="text-dark">Elchin Aliyev</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
<%--                            <a class="dropdown-item" href="pages-profile.html"><i class="align-middle me-1"--%>
<%--                                                                                  data-feather="user"></i> file</a>--%>
                            <a class="dropdown-item" href="#employeePerformance"><i class="align-middle me-1" data-feather="pie-chart"></i>
                                Analytics</a>
                            <div class="dropdown-divider"></div>
<%--                            <a class="dropdown-item" href="pages-settings.html"><i class="align-middle me-1"--%>
<%--                                                                                   data-feather="settings"></i> Settings--%>
<%--                                &--%>
<%--                                Privacy</a>--%>
<%--                            <a class="dropdown-item" href="#"><i class="align-middle me-1"--%>
<%--                                                                 data-feather="help-circle"></i> Help Center</a>--%>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <main class="content" id="employeePerformance">
            <div class="container-fluid p-0">

                <div class="row mb-2 mb-xl-3">
                    <div class="col-auto d-none d-sm-block">
                        <h3>Employee Performance</h3>
                    </div>

<%--                    <div class="col-auto ms-auto text-end mt-n1">--%>
<%--                        <nav aria-label="breadcrumb">--%>
<%--                            <ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">--%>
<%--                                <li class="breadcrumb-item"><a href="#">Employees</a></li>--%>
<%--                                <li class="breadcrumb-item"><a href="#">Candidates</a></li>--%>
<%--                            </ol>--%>
<%--                        </nav>--%>
<%--                    </div>--%>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-xxl-5 d-flex">
                        <div class="w-100">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col mt-0">
                                                    <h5 class="card-title">Employee Success Rate</h5>
                                                </div>

<%--                                                <div class="col-auto">--%>
<%--                                                    <div class="avatar">--%>
<%--                                                        <div class="avatar-title rounded-circle bg-primary-light">--%>
<%--                                                            <i class="align-middle" data-feather="users"></i>--%>
<%--                                                        </div>--%>
<%--                                                    </div>--%>
<%--                                                </div>--%>
                                            </div>
                                            <h1 class="mt-1 mb-3">90%</h1>
                                            <div class="mb-0">
                                                <span class="text-success"> <i class="mdi mdi-arrow-bottom-right"></i> +10% </span>
                                                <span class="text-muted">Since last month</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col mt-0">
                                                    <h5 class="card-title">Suggests</h5>
                                                </div>

<%--                                                <div class="col-auto">--%>
<%--                                                    <div class="avatar">--%>
<%--                                                        <div class="avatar-title rounded-circle bg-primary-light">--%>
<%--                                                            <i class="align-middle" data-feather=""></i>--%>
<%--                                                        </div>--%>
<%--                                                    </div>--%>
<%--                                                </div>--%>
                                            </div>
                                            <h1 class="mt-1 mb-3">27</h1>
                                            <div class="mb-0">
                                                <span class="text-success"> <i class="mdi mdi-arrow-bottom-right"></i> 5% </span>
                                                <span class="text-muted">Since last month</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col mt-0">
                                                    <h5 class="card-title">Number of Employees </h5>
                                                </div>

<%--                                                <div class="col-auto">--%>
<%--                                                    <div class="avatar">--%>
<%--                                                        <div class="avatar-title rounded-circle bg-primary-light">--%>
<%--                                                            <i class="align-middle" data-feather="users"></i>--%>
<%--                                                        </div>--%>
<%--                                                    </div>--%>
<%--                                                </div>--%>
                                            </div>
                                            <h1 class="mt-1 mb-3">170</h1>
                                            <div class="mb-0">
                                                <span class="text-success"> <i class="mdi mdi-arrow-bottom-right"></i> +6% </span>
                                                <span class="text-muted">Since last month</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col mt-0">
                                                    <h5 class="card-title">Complaints</h5>
                                                </div>

<%--                                                <div class="col-auto">--%>
<%--                                                    <div class="avatar">--%>
<%--                                                        <div class="avatar-title rounded-circle bg-primary-light">--%>
<%--                                                            <i class="align-middle" data-feather="users"></i>--%>
<%--                                                        </div>--%>
<%--                                                    </div>--%>
<%--                                                </div>--%>
                                            </div>
                                            <h1 class="mt-1 mb-3">16</h1>
                                            <div class="mb-0">
                                                <span class="text-danger"> <i class="mdi mdi-arrow-bottom-right"></i> -10% </span>
                                                <span class="text-muted">Since last month</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 col-xxl-7">
                        <div class="card flex-fill w-100">
                            <div class="card-header">
                                <div class="card-actions float-end">
                                    <div class="dropdown show">
                                        <a href="#" data-bs-toggle="dropdown" data-bs-display="static">
                                            <i class="align-middle" data-feather="more-horizontal"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="card-title mb-0">Average Success Rate of Employee Performance</h5>
                            </div>
                            <div class="card-body py-3">
                                <div class="chart chart-sm">
                                    <canvas id="chartjs-dashboard-line"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<%--                <div class="row">--%>
<%--                    <div class="col-12 col-md-6 col-xxl-3 d-flex order-2 order-xxl-3">--%>
<%--                        <div class="card flex-fill w-100">--%>
<%--                            <div class="card-header">--%>
<%--                                <div class="card-actions float-end">--%>
<%--                                    <div class="dropdown show">--%>
<%--                                        <a href="#" data-bs-toggle="dropdown" data-bs-display="static">--%>
<%--                                            <i class="align-middle" data-feather="more-horizontal"></i>--%>
<%--                                        </a>--%>

<%--                                        <div class="dropdown-menu dropdown-menu-end">--%>
<%--                                            <a class="dropdown-item" href="#">Action</a>--%>
<%--                                            <a class="dropdown-item" href="#">Another action</a>--%>
<%--                                            <a class="dropdown-item" href="#">Something else here</a>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                                <h5 class="card-title mb-0">Browser Usage</h5>--%>
<%--                            </div>--%>
<%--                            <div class="card-body d-flex">--%>
<%--                                <div class="align-self-center w-100">--%>
<%--                                    <div class="py-3">--%>
<%--                                        <div class="chart chart-xs">--%>
<%--                                            <canvas id="chartjs-dashboard-pie"></canvas>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>

<%--                                    <table class="table mb-0">--%>
<%--                                        <tbody>--%>
<%--                                        <tr>--%>
<%--                                            <td><i class="fas fa-circle text-primary fa-fw"></i> Chrome</td>--%>
<%--                                            <td class="text-end">4306</td>--%>
<%--                                        </tr>--%>
<%--                                        <tr>--%>
<%--                                            <td><i class="fas fa-circle text-warning fa-fw"></i> Firefox</td>--%>
<%--                                            <td class="text-end">3801</td>--%>
<%--                                        </tr>--%>
<%--                                        <tr>--%>
<%--                                            <td><i class="fas fa-circle text-danger fa-fw"></i> IE</td>--%>
<%--                                            <td class="text-end">1689</td>--%>
<%--                                        </tr>--%>
<%--                                        </tbody>--%>
<%--                                    </table>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                    <div class="col-12 col-md-12 col-xxl-6 d-flex order-3 order-xxl-2">--%>
<%--                        <div class="card flex-fill w-100">--%>
<%--                            <div class="card-header">--%>
<%--                                <div class="card-actions float-end">--%>
<%--                                    <div class="dropdown show">--%>
<%--                                        <a href="#" data-bs-toggle="dropdown" data-bs-display="static">--%>
<%--                                            <i class="align-middle" data-feather="more-horizontal"></i>--%>
<%--                                        </a>--%>

<%--                                        <div class="dropdown-menu dropdown-menu-end">--%>
<%--                                            <a class="dropdown-item" href="#">Action</a>--%>
<%--                                            <a class="dropdown-item" href="#">Another action</a>--%>
<%--                                            <a class="dropdown-item" href="#">Something else here</a>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                                <h5 class="card-title mb-0">Real-Time</h5>--%>
<%--                            </div>--%>
<%--                            <div class="card-body px-4">--%>
<%--                                <div id="world_map" style="height:350px;"></div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                    <div class="col-12 col-md-6 col-xxl-3 d-flex order-1 order-xxl-1">--%>
<%--                        <div class="card flex-fill">--%>
<%--                            <div class="card-header">--%>
<%--                                <div class="card-actions float-end">--%>
<%--                                    <div class="dropdown show">--%>
<%--                                        <a href="#" data-bs-toggle="dropdown" data-bs-display="static">--%>
<%--                                            <i class="align-middle" data-feather="more-horizontal"></i>--%>
<%--                                        </a>--%>

<%--                                        <div class="dropdown-menu dropdown-menu-end">--%>
<%--                                            <a class="dropdown-item" href="#">Action</a>--%>
<%--                                            <a class="dropdown-item" href="#">Another action</a>--%>
<%--                                            <a class="dropdown-item" href="#">Something else here</a>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                                <h5 class="card-title mb-0">Calendar</h5>--%>
<%--                            </div>--%>
<%--                            <div class="card-body d-flex">--%>
<%--                                <div class="align-self-center w-100">--%>
<%--                                    <div class="chart">--%>
<%--                                        <div id="datetimepicker-dashboard"></div>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>

                <div class="row">
                    <div class="col-12 col-lg-8 col-xxl-9 d-flex">
                        <div class="card flex-fill">
                            <div class="card-header">
                                <div class="card-actions float-end">
                                    <div class="dropdown show">
                                        <a href="#" data-bs-toggle="dropdown" data-bs-display="static">
                                            <i class="align-middle" data-feather="more-horizontal"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="card-title mb-0">Last Month Tasks</h5>
                            </div>
                            <table class="table table-hover my-0">
                                <thead>
                                <tr>
                                    <th>Task</th>
                                    <th class="d-none d-xl-table-cell">Start Date</th>
                                    <th class="d-none d-xl-table-cell">End Date</th>
                                    <th>Status</th>
                                    <th class="d-none d-md-table-cell">Assignee</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Renew Website Design</td>
                                    <td class="d-none d-xl-table-cell">01/05/2021</td>
                                    <td class="d-none d-xl-table-cell">31/05/2021</td>
                                    <td><span class="badge bg-success">Done</span></td>
                                    <td class="d-none d-md-table-cell">Elçin Əliyev</td>
                                </tr>
                                <tr>
                                    <td>New Marketing Strategy</td>
                                    <td class="d-none d-xl-table-cell">01/05/2021</td>
                                    <td class="d-none d-xl-table-cell">31/05/2021</td>
                                    <td><span class="badge bg-danger">Cancelled</span></td>
                                    <td class="d-none d-md-table-cell">Pervin Pashazada</td>
                                </tr>
                                <tr>
                                    <td>Report About Company Benefit</td>
                                    <td class="d-none d-xl-table-cell">01/05/2021</td>
                                    <td class="d-none d-xl-table-cell">31/05/2021</td>
                                    <td><span class="badge bg-success">Done</span></td>
                                    <td class="d-none d-md-table-cell">Azər Orucov</td>
                                </tr>
                                <tr>
                                    <td>Report About Company Loss</td>
                                    <td class="d-none d-xl-table-cell">01/05/2021</td>
                                    <td class="d-none d-xl-table-cell">31/05/2021</td>
                                    <td><span class="badge bg-warning">In progress</span></td>
                                    <td class="d-none d-md-table-cell">Narmin Musazada</td>
                                </tr>
                                <tr>
                                    <td>Select New Candidates</td>
                                    <td class="d-none d-xl-table-cell">01/05/2021</td>
                                    <td class="d-none d-xl-table-cell">31/05/2021</td>
                                    <td><span class="badge bg-success">Done</span></td>
                                    <td class="d-none d-md-table-cell">Mirvari Yusifova</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 col-xxl-3 d-flex">
                        <div class="card flex-fill w-100">
                            <div class="card-header">
                                <div class="card-actions float-end">
                                    <div class="dropdown show">
                                        <a href="#" data-bs-toggle="dropdown" data-bs-display="static">
                                            <i class="align-middle" data-feather="more-horizontal"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="card-title mb-0">Suggests/Complaints</h5>
                            </div>
                            <div class="card-body d-flex w-100">
                                <div class="align-self-center chart chart-lg">
                                    <canvas id="chartjs-dashboard-bar"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </main>
        <section id="employeesTable">
            <div class="col-md-10 col-md-offset-5 text-center">
                <div class="section-heading">
                    <h2>Staff</h2>
                </div>
            </div>
            <table id="employeeTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>

                    <th>Name</th>
                    <th>Surname</th>
                    <th>Salary</th>
                    <th>Position</th>
                    <th>Department</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${allEmployeeDataProjection}" var="employeeData">
                    <tr>
                        <td><c:out value="${employeeData.name}"/></td>
                        <td><c:out value="${employeeData.surname}"/></td>
                        <td><c:out value="${employeeData.salary}"/></td>
                        <td><c:out value="${employeeData.position}"/></td>
                        <td><c:out value="${employeeData.departmentName}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </section>
        <section id="candidatesTable">
            <div class="col-md-10 col-md-offset-5 text-center">
                <div class="section-heading">
                    <h2>Candidates</h2>
                </div>
            </div>
            <table id="candidateTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>

                    <th>Name</th>
                    <th>Surname</th>
                    <th>Mail</th>
                    <th>Phone</th>
                    <th>Applied Position</th>
                    <th>Applied Date</th>
                    <th>View Resume</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${allApplicationForms}" var="applicationForm">
                    <tr>
                        <td><c:out value="${applicationForm.name}"/></td>
                        <td><c:out value="${applicationForm.surname}"/></td>
                        <td><c:out value="${applicationForm.mail}"/></td>
                        <td><c:out value="${applicationForm.phone}"/></td>
                        <td><c:out value="${applicationForm.positionName}"/></td>
                        <td><c:out value="${applicationForm.created}"/></td>
                        <td>
                            <a href="/hr/resume/<c:out value="${applicationForm.id}"/>" target="_blank">
                                <button type="button" class="btn btn-primary">Resume</button>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </section>
        <section id="interviewsTable">
            <div class="col-md-10 col-md-offset-5 text-center">
                <div class="section-heading">
                    <h2>Interviews</h2>
                </div>
            </div>
            <table id="interviewTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Mail</th>
                    <th>Phone</th>
                    <th>Interview Date</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${allInterviews}" var="interview">
                    <tr>
                        <td><c:out value="${interview.name}"/></td>
                        <td><c:out value="${interview.surname}"/></td>
                        <td><c:out value="${interview.mail}"/></td>
                        <td><c:out value="${interview.phone}"/></td>
                        <td><c:out value="${interview.date}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </section>

        <section id="suggestsComplaints">
            <div class="col-md-10 col-md-offset-5 text-center">
                <div class="section-heading">
                    <h2>Suggests / Complaints</h2>
                </div>
            </div>
            <table id="suggestTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>

                    <th>Type</th>
                    <th>Report Text</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${allReports}" var="report">
                    <tr>
                        <td><c:out value="${report.type}"/></td>
                        <td><c:out value="${report.text}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </section>

        <br/><br/>
        <section id="newIntervyu">
            <div class="col-md-10 col-md-offset-5 text-center">
                <div class="section-heading">
                    <h2>Invite Candidate</h2>
                </div>
            </div>

            <%--Intervyu Modal--%>
            <div>
                <div>
                    <div>

                        <div >

                            <div class="row">
                                <div class="col-md-6">

                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="name">Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" height="20px">

                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-6">

                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="surname">Surname <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="surname" name="surname"
                                                   placeholder="Surname">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phoneId">Phone Number <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" id="phoneId" name="phoneId"
                                               placeholder="Phone Number">
                                    </div>

                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="emailAddress">Mail Address<span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" id="emailAddress" name="emailAddress"
                                               placeholder="Mail Address">
                                    </div>

                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="intervyuDate">Interview Date<span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="intervyuDate" name="intervyuDate"
                                               placeholder="Interview Date">
                                    </div>

                                    <!-- /.form-group -->
                                </div>

                            </div>
                            <div class="modal-footer">
                                <%--                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Bağla</button>--%>
                                <!-- <button type="button" id="search" class="btn btn-primary">Axtar</button>-->
                                <button type="button" id="save" class="btn btn-primary">Invite</button>


                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

            </div>


            <%--Intervyu Modal --%>
        </section>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row text-muted">
                    <div class="col-6 text-start">
                        <p class="mb-0">
                            <a href="index.html" class="text-muted"><strong>HR System</strong></a> &copy;
                        </p>
                    </div>
<%--                    <div class="col-6 text-end">--%>
<%--                        <ul class="list-inline">--%>
<%--                            <li class="list-inline-item">--%>
<%--                                <a class="text-muted" href="#">Support</a>--%>
<%--                            </li>--%>
<%--                            <li class="list-inline-item">--%>
<%--                                <a class="text-muted" href="#">Help Center</a>--%>
<%--                            </li>--%>
<%--                            <li class="list-inline-item">--%>
<%--                                <a class="text-muted" href="#">Privacy</a>--%>
<%--                            </li>--%>
<%--                            <li class="list-inline-item">--%>
<%--                                <a class="text-muted" href="#">Terms</a>--%>
<%--                            </li>--%>
<%--                        </ul>--%>
<%--                    </div>--%>
                </div>
            </div>
        </footer>
    </div>
</div>

<script src="/hr/js/app.js"></script>



<script src="/index/assets/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/index/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        var ctx = document.getElementById("chartjs-dashboard-line").getContext("2d");
        var gradient = ctx.createLinearGradient(0, 0, 0, 225);
        gradient.addColorStop(0, "rgba(215, 227, 244, 1)");
        gradient.addColorStop(1, "rgba(215, 227, 244, 0)");
        // Line chart
        new Chart(document.getElementById("chartjs-dashboard-line"), {
            type: "line",
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                    label: "Performance (%)",
                    fill: true,
                    backgroundColor: gradient,
                    borderColor: window.theme.primary,
                    data: [
                        70,
                        80,
                        90,
                        85,
                        90,
                        80,
                        95,
                        90,
                        85,
                        90,
                        80,
                        90
                    ]
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    intersect: false
                },
                hover: {
                    intersect: true
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                scales: {
                    xAxes: [{
                        reverse: true,
                        gridLines: {
                            color: "rgba(0,0,0,0.0)"
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            stepSize: 0
                        },
                        display: true,
                        borderDash: [3, 3],
                        gridLines: {
                            color: "rgba(0,0,0,0.0)"
                        }
                    }]
                }
            }
        });
    });
</script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        // Pie chart
        new Chart(document.getElementById("chartjs-dashboard-pie"), {
            type: "pie",
            data: {
                labels: ["Chrome", "Firefox", "IE"],
                datasets: [{
                    data: [4306, 3801, 1689],
                    backgroundColor: [
                        window.theme.primary,
                        window.theme.warning,
                        window.theme.danger
                    ],
                    borderWidth: 5
                }]
            },
            options: {
                responsive: !window.MSInputMethodContext,
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                cutoutPercentage: 75
            }
        });
    });
</script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        // Bar chart
        new Chart(document.getElementById("chartjs-dashboard-bar"), {
            type: "bar",
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                    label: "This year",
                    backgroundColor: window.theme.primary,
                    borderColor: window.theme.primary,
                    hoverBackgroundColor: window.theme.primary,
                    hoverBorderColor: window.theme.primary,
                    data: [54, 67, 41, 55, 62, 45, 55, 73, 60, 76, 48, 79],
                    barPercentage: .75,
                    categoryPercentage: .5
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: false
                        },
                        stacked: false,
                        ticks: {
                            stepSize: 20
                        }
                    }],
                    xAxes: [{
                        stacked: false,
                        gridLines: {
                            color: "transparent"
                        }
                    }]
                }
            }
        });
    });
</script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        var markers = [{
            coords: [31.230391, 121.473701],
            name: "Shanghai"
        },
            {
                coords: [28.704060, 77.102493],
                name: "Delhi"
            },
            {
                coords: [6.524379, 3.379206],
                name: "Lagos"
            },
            {
                coords: [35.689487, 139.691711],
                name: "Tokyo"
            },
            {
                coords: [23.129110, 113.264381],
                name: "Guangzhou"
            },
            {
                coords: [40.7127837, -74.0059413],
                name: "New York"
            },
            {
                coords: [34.052235, -118.243683],
                name: "Los Angeles"
            },
            {
                coords: [41.878113, -87.629799],
                name: "Chicago"
            },
            {
                coords: [51.507351, -0.127758],
                name: "London"
            },
            {
                coords: [40.416775, -3.703790],
                name: "Madrid "
            }
        ];
        var map = new jsVectorMap({
            map: "world",
            selector: "#world_map",
            zoomButtons: true,
            markers: markers,
            markerStyle: {
                initial: {
                    r: 9,
                    strokeWidth: 7,
                    stokeOpacity: .4,
                    fill: window.theme.primary
                },
                hover: {
                    fill: window.theme.primary,
                    stroke: window.theme.primary
                }
            },
            zoomOnScroll: false
        });
        window.addEventListener("resize", () => {
            map.updateSize();
        });
    });
</script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        document.getElementById("datetimepicker-dashboard").flatpickr({
            inline: true,
            prevArrow: "<span class=\"fas fa-chevron-left\" title=\"Previous month\"></span>",
            nextArrow: "<span class=\"fas fa-chevron-right\" title=\"Next month\"></span>",
        });
    });
</script>

<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        setTimeout(function () {
            if (localStorage.getItem('popState') !== 'shown') {
                window.notyf.open({
                    type: "success",
                    message: "Get access to all 500+ components and 45+ pages with AdminKit PRO. <u><a class=\"text-white\" href=\"https://adminkit.io/pricing\" target=\"_blank\">More info</a></u> 🚀",
                    duration: 10000,
                    ripple: true,
                    dismissible: false,
                    position: {
                        x: "left",
                        y: "bottom"
                    }
                });

                localStorage.setItem('popState', 'shown');
            }
        }, 15000);
    });
</script>
<script>
    $(document).ready(function () {


        var table = $('#employeeTable').DataTable({});
    });
</script>
<script>
    $(document).ready(function () {


        var table = $('#candidateTable').DataTable({});
    });
</script>

<script>
    $(document).ready(function () {


        var table = $('#suggestTable').DataTable({});
    });
</script>
<script>
    $(document).ready(function () {


        var table = $('#interviewTable').DataTable({});
    });

    $('#save').click(function () {
        var name = $('#name').val();
        var surname = $('#surname').val();
        var emailAddress = $('#emailAddress').val();
        var phone = $('#phoneId').val();
        var intervyuDate = $('#intervyuDate').val();


        var data = {
            name: name,
            surname: surname,
            phone: phone,
            mail: emailAddress,
            intervyuDate: intervyuDate
        };
        if (name.length > 0 && surname.length > 0 && emailAddress.length > 0 && phone.length > 0 && intervyuDate.length > 0) {
            $.ajax({
                url: '/hr/interview',
                type: 'POST',
                data: data,
                success: function (data) {
                    if (data == true) {
                        $('#modal-default').modal('hide');
                        $('#check').modal('show');

                    } else {
                        alert("Xəta baş verdi")
                    }
                }
            })

            $('#name').val('');
            $('#surname').val('');
            $('#emailAddress').val('');
            $('#phoneId').val('');
            $('#intervyuDate').val('');
        }


    });

</script>



</body>


<!-- Mirrored from demo.adminkit.io/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 May 2021 19:15:51 GMT -->
</html>