<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Hr system</title>
    <link rel="stylesheet" href="/index/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/index/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel=stylesheet>

</head>
<body>
<table id="myTable" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Mail</th>
        <th>Phone</th>
        <th>Interview Date</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${allInterviews}" var="interview">
        <tr>
            <td><c:out value="${interview.name}"/></td>
            <td><c:out value="${interview.surname}"/></td>
            <td><c:out value="${interview.mail}"/></td>
            <td><c:out value="${interview.phone}"/></td>
            <td><c:out value="${interview.date}"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<script src="/index/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/index/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/index/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        var table = $('#myTable').DataTable({});
    });
</script>

</body>
</html>
