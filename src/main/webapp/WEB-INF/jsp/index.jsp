<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from onushorit.com/EasyInsurance/live_demo/color_02/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 May 2021 12:08:42 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Easy Insurance - Best Insurance Landing Page Templates</title>

    <link rel="shortcut icon" href="/index/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/index/img/favicon.ico" type="image/x-icon">

    <!-- Bootstrap Core CSS -->
    <link href="/index/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="/index/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/index/assets/simple-line-icons/css/simple-line-icons.css">

    <!-- Preloader CSS -->
    <link rel="stylesheet" href="/index/css/preloader.css">

    <!-- Custom CSS -->
    <link href="/index/css/custom.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="/index/css/responsive.css" rel="stylesheet">

    <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel=stylesheet>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->

</head>

<body id="page-top">

<!-- Preloader -->
<div id="preloader">
    <div id="loading_bar">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div id="mask_cover"></div>
<!-- End Of Preloader -->

<!-- NavBar -->
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top has_menu">
    <div class="container">
        <!-- Brand Logo -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <img src="/index/img/logo.png" alt="LOGO">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="page-scroll" href="#offer">Offers</a>
                </li>
                <li>
                    <a class="page-scroll" href="#about">About</a>
                </li>
                <li>
                    <a class="page-scroll" href="#agents">Staff</a>
                </li>
                <%--                <li>--%>
                <%--                    <a class="page-scroll" href="#faq">FAQ</a>--%>
                <%--                </li>--%>
<%--                                <li>--%>
<%--                                    <a class="page-scroll" href="#blog">Query</a>--%>
<%--                                </li>--%>
                <li>
                    <a class="page-scroll" href="#contact">Contact </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Of Nav Bar -->

<!-- Header -->
<header id="page_top" class="home">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="header-content">
                    <div class="header-content-inner">
                        <h1>get insured and <br>feel secrued</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                        <a href="#quote" class="btn btn-outline btn-xl page-scroll">Get Quote Now</a>
                        <a href="#quote" class="page-scroll scroll-down"><i class="fa fa-angle-double-down"
                                                                            aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Of Header -->

<!-- Section Quote -->
<section id="quote" class="quote">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <div class="section-heading">
                    <h2>Get Quote Now</h2>
                    <p>It's Free and Faster. Your details will be secure with us. </p>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="form-container">
                    <div class="form-mockup">
                        <form action="http://onushorit.com/EasyInsurance/live_demo/color_02/lead.php" method="post">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="Type Name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" placeholder="Type E-mail" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="phone" class="form-control" placeholder="Type Phone Number" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" id="autocomplete" name="city" class="form-control" placeholder="Type Your City Name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="zip_code" class="form-control" placeholder="Type Zip Code" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select class="form-control" name="insurance" required>
                                            <option value="">Select Your Need</option>
                                            <option value="Home">Home Insurance</option>
                                            <option value="Health">Health Insurance</option>
                                            <option value="Travel">Travel Insurance</option>
                                            <option value="Life">Life Insurance</option>
                                            <option value="Business">Business Insurance</option>
                                            <option value="Vehicle">Vehicle Insurance</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default quote_btn">GET QUOTE</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Of Section Quote -->

<!-- Section Offer -->
<section id="offer" class="offer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <div class="section-heading">
                    <h2>What We Offer</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                        totam rem aperiam. </p>
                </div>
            </div>
        </div>
        <div class="row text-center">

            <div class="col-md-4 col-sm-6">
                <div class="feature-item">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <h3>Home Insurance</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna.</p>
                    <a href="#quote" class="btn btn-primary page-scroll">Get Quote <i
                            class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="feature-item">
                    <i class="fa fa-medkit" aria-hidden="true"></i>
                    <h3>Health Insurance</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna.</p>
                    <a href="#quote" class="btn btn-primary page-scroll">Get Quote <i
                            class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="feature-item">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                    <h3>Travel Insurance</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna.</p>
                    <a href="#quote" class="btn btn-primary page-scroll">Get Quote <i
                            class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="feature-item last-one">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                    <h3>Life Insurance</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna.</p>
                    <a href="#quote" class="btn btn-primary page-scroll">Get Quote <i
                            class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="feature-item">
                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                    <h3>Business Insurance</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna.</p>
                    <a href="#quote" class="btn btn-primary page-scroll">Get Quote <i
                            class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="feature-item last-one">
                    <i class="fa fa-car" aria-hidden="true"></i>
                    <h3>Vehicle Insurance</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna.</p>
                    <a href="#quote" class="btn btn-primary page-scroll">Get Quote <i
                            class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Of Section Offer -->

<!-- Section About -->
<section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <div class="section-heading">
                    <h2>About Us</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                        totam rem aperiam. </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 about_left">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="feature-item text-center">
                                <img src="/index/img/about.jpg" alt="About Image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 about_right">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="feature-item">
                                <!-- Timeline Start -->
                                <div class="timeline-about">

                                    <!-- Single Timeline -->
                                    <article class="timeline-entry">
                                        <div class="timeline-entry-inner">
                                            <div class="timeline-icon bg-color">
                                                <i class="entypo-feather"></i>
                                            </div>
                                            <div class="timeline-label">
                                                <h2>December, 1989</h2>
                                                <p>Our company was founded in 1989, with a few professional agents and a
                                                    small office.</p>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Single Timeline End -->

                                    <!-- Single Timeline -->
                                    <article class="timeline-entry">
                                        <div class="timeline-entry-inner">
                                            <div class="timeline-icon bg-color">
                                                <i class="entypo-suitcase"></i>
                                            </div>
                                            <div class="timeline-label">
                                                <h2>February, 1996</h2>
                                                <p>We open two <strong>New Branches</strong>, <strong>65</strong>
                                                    Insurance agents, <strong>3,000</strong> plus Clients.</p>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Single Timeline End -->

                                    <!-- Single Timeline -->
                                    <article class="timeline-entry">
                                        <div class="timeline-entry-inner">
                                            <div class="timeline-icon bg-color">
                                                <i class="entypo-camera"></i>
                                            </div>
                                            <div class="timeline-label">
                                                <h2>March, 2006</h2>
                                                <p>Open Five <strong>More Branches</strong>, <strong>100</strong> plus
                                                    agents, <strong>10,000</strong> plus Clients.</p>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Single Timeline End -->

                                    <!-- Single Timeline -->
                                    <article class="timeline-entry">
                                        <div class="timeline-entry-inner">
                                            <div class="timeline-icon bg-color">
                                                <i class="entypo-camera"></i>
                                            </div>
                                            <div class="timeline-label">
                                                <h2>July, 2017</h2>
                                                <p>Become the <strong>Best</strong> Insurance Company in
                                                    <strong>USA</strong>.</p>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Single Timeline End -->

                                </div>
                                <!-- Timeline End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Of Section About -->

<!-- Section Counter -->
<section id="counter" class="counter_up text-center">
    <div class="container">
        <div class="row">
            <div class="main_counter_content text-center white-text wow fadeInUp">
                <div class="col-md-3 col-sm-6">
                    <div class="single_counter">
                        <p class="counter_icon"><i class="icon-people"></i></p>
                        <span class="counter">500</span>K
                        <p>Satisfied Clients</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single_counter">
                        <p class="counter_icon"><i class="fa fa-usd" aria-hidden="true"></i></p>
                        <span class="counter">550</span>K
                        <p>Insurance</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single_counter">
                        <p class="counter_icon"><i class="icon-user-following"></i></p>
                        <span class="counter">150</span>K
                        <p>Repeat Client</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single_counter">
                        <p class="counter_icon"><i class="icon-check"></i></p>
                        <span class="counter">100</span>%
                        <p>Safe & Secure</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Counter -->

<!-- Section Agents -->
<section id="agents" class="agents text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <div class="section-heading">
                    <h2>Our Staff</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <table id="myTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>

                    <th>Name</th>
                    <th>Surname</th>
                    <th>Position</th>
                    <th>Department</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${allEmployeeDataProjection}" var="employeeData">
                    <tr>
                        <td><c:out value="${employeeData.name}"/></td>
                        <td><c:out value="${employeeData.surname}"/></td>
                        <td><c:out value="${employeeData.position}"/></td>
                        <td><c:out value="${employeeData.departmentName}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <div class="section-heading">
                    <h2>Apply Now</h2>
<%--                    <p>It's Free and Faster. Your details will be secure with us. </p>--%>
                    <p/>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="form-container">
                    <div class="form-mockup">
                        <form action="/apply/job" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="First"
                                               required/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="surname" class="form-control" placeholder="Last"
                                               required/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Type Your Phone"
                                               name="phone" required/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Type Mail Address"
                                               name="mail" required/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select name="position" class="form-control" required>
                                            <option value="">Select Your Applied Position</option>
                                            <option value="Human Resource Specialist">Human Resource Specialist</option>
                                            <option value="Software Engineer">Software Engineer</option>
                                            <option value="Marketing Specialist">Marketing Specialist</option>
                                            <option value="Finance Specialist">Finance Specialist</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="file" name="file" accept="file/*" class="form-control">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default quote_btn">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<br/><br/>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <div class="section-heading">
                    <h2>Report</h2>
                </div>
            </div>
        </div>
        <br/><br/>
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="form-container">
                    <div class="form-mockup">
                        <form action="/report" method="post" enctype="application/x-www-form-urlencoded">
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select name="type" class="form-control" required>
                                            <option value="">Select Your Report Type</option>
                                            <option value="Suggest">Suggest</option>
                                            <option value="Complaint">Complaint</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="text" class="form-control" placeholder="Report Here"
                                               required/>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default quote_btn">Submit</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <br/>
    <h3><a href="/performance/query"> Query for Employee Performance Evaluation </a></h3>
</section>
<!-- End Section Agents -->




<!-- Section Testimonials-->
<%--<section id="testimonials" class="testimonials">--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-md-6 col-md-offset-3 text-center">--%>
<%--                <div class="section-heading">--%>
<%--                    <h2>Testimonials</h2>--%>
<%--                    <p>What People Says About Us </p>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="row">--%>
<%--            <div class="col-md-offset-2 col-md-8 text-center">--%>
<%--                <div id="carousel-testimonials" class="carousel slide" data-ride="carousel">--%>
<%--                    <!-- Wrapper for slides -->--%>
<%--                    <div class="carousel-inner">--%>
<%--                        <div class="item active">--%>
<%--                            <div class="row padtb20">--%>
<%--                                <button><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>--%>
<%--                                <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und--%>
<%--                                    Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text--%>
<%--                                    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium--%>
<%--                                    doloremque laudantium, totam rem aperiam.</p>--%>
<%--                                <div class="row">--%>
<%--                                    <div class="col-sm-12">--%>
<%--                                        <img src="/index/img/testimonial/t1.jpg" class="img-circle"--%>
<%--                                             alt="Testimonial Img">--%>
<%--                                    </div>--%>
<%--                                    <div class="col-sm-12 text-center">--%>
<%--                                        <h4><strong>Chelsea G. Goodrich</strong></h4>--%>
<%--                                        <p class="testimonial_subtitle">--%>
<%--                                            <span>CEO</span>, ABC Corporation--%>
<%--                                        </p>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="item">--%>
<%--                            <div class="row padtb20">--%>
<%--                                <button><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>--%>
<%--                                <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und--%>
<%--                                    Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text--%>
<%--                                    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium--%>
<%--                                    doloremque laudantium, totam rem aperiam.</p>--%>
<%--                                <div class="row">--%>
<%--                                    <div class="col-sm-12">--%>
<%--                                        <img src="/index/img/testimonial/t2.jpg" class="img-circle"--%>
<%--                                             alt="Testimonial Img">--%>
<%--                                    </div>--%>
<%--                                    <div class="col-sm-12 text-center">--%>
<%--                                        <h4><strong>Thomas V. Bass</strong></h4>--%>
<%--                                        <p class="testimonial_subtitle">--%>
<%--                                            <span>Admin</span>, LMN Corporation--%>
<%--                                        </p>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="item">--%>
<%--                            <div class="row padtb20">--%>
<%--                                <button><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>--%>
<%--                                <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und--%>
<%--                                    Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text--%>
<%--                                    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium--%>
<%--                                    doloremque laudantium, totam rem aperiam.</p>--%>
<%--                                <div class="row">--%>
<%--                                    <div class="col-sm-12">--%>
<%--                                        <img src="/index/img/testimonial/t3.jpg" class="img-circle"--%>
<%--                                             alt="Testimonial Img">--%>
<%--                                    </div>--%>
<%--                                    <div class="col-sm-12 text-center">--%>
<%--                                        <h4><strong>Kevin C. Lang</strong></h4>--%>
<%--                                        <p class="testimonial_subtitle">--%>
<%--                                            <span>CEO</span>, XYZ Corporation--%>
<%--                                        </p>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>

<%--                <!-- ***Testimonial Carousel Control. If You Like To Use Control Just Remove Comment Tag*** -->--%>
<%--                <!-- <div class="controls testimonial_control text-center">--%>
<%--                    <a class="left fa fa-long-arrow-left btn btn-default testimonial_btn" href="#carousel-testimonials"--%>
<%--                      data-slide="prev"></a>--%>

<%--                    <a class="right fa fa-long-arrow-right btn btn-default testimonial_btn" href="#carousel-testimonials"--%>
<%--                      data-slide="next"></a>--%>
<%--                </div> -->--%>
<%--                <!-- End Testimonial Carousel Control -->--%>

<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</section>--%>
<!-- End Of Section Testimonials -->

<!-- Section FAQ-->
<%--<section id="faq" class="faq">--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-md-6 col-md-offset-3 text-center">--%>
<%--                <div class="section-heading">--%>
<%--                    <h2>FAQ</h2>--%>
<%--                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,--%>
<%--                        totam rem aperiam. </p>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="row section-separator">--%>
<%--            <div class="faq-items">--%>
<%--                <div class="col-md-6">--%>
<%--                    <div class="faqs">--%>
<%--                        <h4>Do I have to pay for an online quote?</h4>--%>
<%--                        <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist--%>
<%--                            in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus--%>
<%--                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae--%>
<%--                            ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo--%>
<%--                            en.</p>--%>
<%--                        <a class="" href="#">Learn More<i class="fa  fa-long-arrow-right"></i></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="col-md-6">--%>
<%--                    <div class="faqs">--%>
<%--                        <h4>Do I have to pay for an online quote?</h4>--%>
<%--                        <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist--%>
<%--                            in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus--%>
<%--                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae--%>
<%--                            ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo--%>
<%--                            en.</p>--%>
<%--                        <a class="" href="#">Learn More<i class="fa  fa-long-arrow-right"></i></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="row section-separator">--%>
<%--            <div class="faq-items">--%>
<%--                <div class="col-md-6">--%>
<%--                    <div class="faqs">--%>
<%--                        <h4>Do I have to pay for an online quote?</h4>--%>
<%--                        <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist--%>
<%--                            in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus--%>
<%--                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae--%>
<%--                            ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo--%>
<%--                            en.</p>--%>
<%--                        <a class="" href="#">Learn More<i class="fa  fa-long-arrow-right"></i></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="col-md-6">--%>
<%--                    <div class="faqs">--%>
<%--                        <h4>Do I have to pay for an online quote?</h4>--%>
<%--                        <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist--%>
<%--                            in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus--%>
<%--                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae--%>
<%--                            ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo--%>
<%--                            en.</p>--%>
<%--                        <a class="" href="#">Learn More<i class="fa  fa-long-arrow-right"></i></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</section>--%>
<!-- End Of Section FAQ -->

<!-- Section Newsletter -->
<section id="newsletter" class="newsletter text-center">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="news_subs">
                    <div class="single">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        <h2>Subscribe to our Newsletter</h2>
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Enter your email">
                            <span class="input-group-btn">
                                <button class="btn btn-theme" type="submit">Subscribe</button>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-5">
                <div class="get_call">
                    <div class="single">
                        <i class="fa fa-phone-square" aria-hidden="true"></i>
                        <h2>Submit Your Number for get a call</h2>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter your number">
                            <span class="input-group-btn">
                                <button class="btn btn-theme" type="submit">Submit</button>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Of Section Newsletter -->

<!-- Section Blog -->
<%--<section id="blog" class="blog">--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-md-6 col-md-offset-3 text-center">--%>
<%--                <div class="section-heading">--%>
<%--                    <h2>Our Blog</h2>--%>
<%--                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,--%>
<%--                        totam rem aperiam. </p>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="row">--%>
<%--            <div class="col-md-4 col-sm-12">--%>
<%--                <div class="blog_worp">--%>
<%--                    <div class="blog_img">--%>
<%--                        <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01"--%>
<%--                                                          src="/index/img/blog/blog_01.jpg"></a>--%>
<%--                    </div>--%>
<%--                    <div class="blog_content">--%>
<%--                        <h2><a class="title" href="#">eaque ipsa quae ab illo inventore veritatis et.</a></h2>--%>
<%--                        <h4>--%>
<%--                            <span class="post_date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Jan 19, 2017</span>--%>
<%--                            <span class="post_comment"><i class="fa fa-eye" aria-hidden="true"></i> 467 Views</span>--%>
<%--                        </h4>--%>
<%--                        <p>Lorem ipsum dolor sitamet secteture adipiscing eilit mauris diem lorem ipsum anibus atdsim--%>
<%--                            eipit.</p>--%>
<%--                        <a class="btn-link" href="#">Learn More <i class="fa  fa-long-arrow-right"--%>
<%--                                                                   aria-hidden="true"></i></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <div class="col-md-4 col-sm-12 mar-20t">--%>
<%--                <div class="blog_worp">--%>
<%--                    <div class="blog_img">--%>
<%--                        <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01"--%>
<%--                                                          src="/index/img/blog/blog_02.jpg"></a>--%>
<%--                    </div>--%>
<%--                    <div class="blog_content">--%>
<%--                        <h2><a class="title" href="#">eaque ipsa quae ab illo inventore veritatis et.</a></h2>--%>
<%--                        <h4>--%>
<%--                            <span class="post_date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Feb 14, 2017</span>--%>
<%--                            <span class="post_comment"><i class="fa fa-eye" aria-hidden="true"></i> 249 Views</span>--%>
<%--                        </h4>--%>
<%--                        <p>Lorem ipsum dolor sitamet secteture adipiscing eilit mauris diem lorem ipsum anibus atdsim--%>
<%--                            eipit. </p>--%>
<%--                        <a class="btn-link" href="#">Learn More <i class="fa  fa-long-arrow-right"--%>
<%--                                                                   aria-hidden="true"></i></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <div class="col-md-4 col-sm-12 mar-20t">--%>
<%--                <div class="blog_worp">--%>
<%--                    <div class="blog_img">--%>
<%--                        <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01"--%>
<%--                                                          src="/index/img/blog/blog_03.jpg"></a>--%>
<%--                    </div>--%>
<%--                    <div class="blog_content">--%>
<%--                        <h2><a class="title" href="#">eaque ipsa quae ab illo inventore veritatis et.</a></h2>--%>
<%--                        <h4>--%>
<%--                            <span class="post_date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Mar 14, 2017</span>--%>
<%--                            <span class="post_comment"><i class="fa fa-eye" aria-hidden="true"></i> 138 Views</span>--%>
<%--                        </h4>--%>
<%--                        <p>Lorem ipsum dolor sitamet secteture adipiscing eilit mauris diem lorem ipsum anibus atdsim--%>
<%--                            eipit.</p>--%>
<%--                        <a class="btn-link" href="#">Learn More <i class="fa  fa-long-arrow-right"--%>
<%--                                                                   aria-hidden="true"></i></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</section>--%>
<!-- End Of Section Blog -->

<!-- Section Client -->
<%--<section id="client" class="client">--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-md-6 col-md-offset-3 text-center">--%>
<%--                <div class="section-heading">--%>
<%--                    <h2>Our Clients</h2>--%>
<%--                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,--%>
<%--                        totam rem aperiam. </p>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="row">--%>
<%--            <div class="col-sm-12">--%>
<%--                <div id="carousel-client" class="carousel slide" data-ride="carousel">--%>
<%--                    <!-- Wrapper for slides -->--%>
<%--                    <div class="carousel-inner">--%>

<%--                        <div class="item active container">--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/1.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/2.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/3.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/4.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/5.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/1.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                        </div>--%>

<%--                        <div class="item container">--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/1.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/2.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/3.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/4.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/5.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/1.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                        </div>--%>

<%--                        <div class="item container">--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/1.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/2.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/3.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/4.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/5.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                            <div class="col-md-2 col-sm-4 col-xs-6">--%>
<%--                                <img class="client_logo img-responsive" src="/index/img/client-logo/1.png"--%>
<%--                                     alt="Client Logo">--%>
<%--                            </div>--%>
<%--                        </div>--%>

<%--                    </div>--%>

<%--                    <!-- Controls -->--%>
<%--                    <div class="mr_top_30 text-center">--%>
<%--                        <a class="left fa fa-long-arrow-left btn btn-default client_btn" href="#carousel-client"--%>
<%--                           role="button" data-slide="prev"></a>--%>
<%--                        <a class="right fa fa-long-arrow-right btn btn-default client_btn" href="#carousel-client"--%>
<%--                           role="button" data-slide="next"></a>--%>
<%--                    </div>--%>
<%--                    <!-- End Controls -->--%>

<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</section>--%>
<!-- End Of Section Client -->

<!-- Section Branches -->
<section id="branche" class="branche">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <div class="section-heading">
                    <h2>Our Branches</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                        totam rem aperiam. </p>
                </div>
            </div>
        </div>
        <div class="row branche_info">
            <div class="col-md-3 col-sm-6">
                <p class="lead">
                    <i class="icon-location-pin"></i>
                    <span>555 Fifth Avenue, <br>New York, NY, 10017</span>
                </p>
            </div>
            <div class="col-md-3 col-sm-6">
                <p class="lead">
                    <i class="icon-location-pin"></i>
                    <span>1754 Clarence Court, <br>Ontario, CA 91762</span>
                </p>
            </div>
            <div class="col-md-3 col-sm-6">
                <p class="lead">
                    <i class="icon-location-pin"></i>
                    <span>1156 Fraggle Drive, <br>Hickory Hills, IL 60457</span>
                </p>
            </div>
            <div class="col-md-3 col-sm-6">
                <p class="lead">
                    <i class="icon-location-pin"></i>
                    <span>2730 New Avenue, <br>Dallas, TX 75204</span>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Of Section Branches -->

<!-- Section Get Quote  Us -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <div class="section-heading">
                    <h2>Contact Us</h2>
                    <p>For any information or help about Insurance you can contact with us. We would love to hear from
                        you! </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
                <form action="http://onushorit.com/EasyInsurance/live_demo/color_02/contact.php" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="InputName" id="InputName"
                                       placeholder="Enter Name" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" class="form-control" id="InputEmail" name="InputEmail"
                                       placeholder="Enter Email" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="InputPhone" name="InputPhone"
                                       placeholder="Enter Phone Number" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="InputZipCode" name="InputZipCode"
                                       placeholder="Enter Zip Code" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5"
                                          placeholder="Type Message Text" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-outline btn-xl page-scroll pull-right">Submit <i
                                        class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- End Of Section Get Quote  Us -->

<!-- Section Call To Action -->
<section id="call_to_action" class="call_to_action">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <div class="section-heading">
                    <h2>Are you ready to work with us ?</h2>
                    <a href="#quote" class="btn btn-outline btn-xl page-scroll">Get Quote Now<i
                            class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Of Section Call To Action -->

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 social_touch text-center">
                <ul class="list-inline list-social">
                    <li class="social-facebook">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="social-twitter">
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="social-fa-linkedin">
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 copy_right text-center">
                <p>Copyright 2017 - EasyInsurance.</p>
            </div>
        </div>
    </div>
</footer>
<!-- End Of Footer -->

<!-- Back To Top -->
<a id="back-to-top" href="#page-top" class="btn btn-primary btn-lg back-to-top page-scroll" role="button"><span
        class="fa fa-long-arrow-up"></span><br>top</a>
<!-- End Back To Top -->


<!-- jQuery -->
<script src="/index/assets/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/index/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/index/js/jquery.easing.js"></script>
<script src="/index/js/jquery.counterup.js"></script>
<script src="/index/js/jquery.waypoints.js"></script>

<!-- Price Slider JavaScript -->
<script src="/index/js/price.slider.js"></script>

<!-- Google API JS -->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDgI2pfq_vtH_hOyGMZ7WM2PGcP72VJbYw"></script>

<!-- Custom JavaScript -->
<script src="/index/js/custom.js"></script>

<script>
    $(document).ready(function () {


        var table = $('#myTable').DataTable({});
    });
</script>

</body>


<!-- Mirrored from onushorit.com/EasyInsurance/live_demo/color_02/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 May 2021 12:09:26 GMT -->
</html>
