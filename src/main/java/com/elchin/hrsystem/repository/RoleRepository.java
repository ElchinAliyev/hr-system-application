package com.elchin.hrsystem.repository;

import com.elchin.hrsystem.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
