package com.elchin.hrsystem.repository;

import com.elchin.hrsystem.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> getUserByUsernameAndState(String username, int state);
}
