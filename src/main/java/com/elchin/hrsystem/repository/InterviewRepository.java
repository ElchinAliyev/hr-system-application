package com.elchin.hrsystem.repository;

import com.elchin.hrsystem.model.Interview;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InterviewRepository extends JpaRepository<Interview, Integer> {
}
