package com.elchin.hrsystem.repository;

import com.elchin.hrsystem.model.Employee;
import com.elchin.hrsystem.repository.projection.EmployeeDataProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {


    @Query("select e.id as id , e.name as name , e.surname as surname , e.salary as salary, e.position as position," +
            " d.name as departmentName from Employee e " +
            "join e.department d")
    List<EmployeeDataProjection> getAllEmployeeDataProjection();
}