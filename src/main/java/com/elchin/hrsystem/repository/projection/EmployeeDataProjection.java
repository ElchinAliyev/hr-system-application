package com.elchin.hrsystem.repository.projection;

import java.math.BigDecimal;

public interface EmployeeDataProjection {

    int getId();

    String getName();

    String getSurname();

    String getPosition();

    String getDepartmentName();

    BigDecimal getSalary();

}
