package com.elchin.hrsystem.repository;

import com.elchin.hrsystem.model.ApplicationForm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ApplicationFormRepository extends JpaRepository<ApplicationForm, Integer> {

    Optional<ApplicationForm> getApplicationFormById(int state);
}
