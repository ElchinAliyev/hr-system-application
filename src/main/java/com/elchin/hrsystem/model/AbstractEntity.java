package com.elchin.hrsystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
public abstract class AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "created", updatable = false)
    private LocalDateTime createDate;

    @Column(name = "updated")
    private LocalDateTime updateDate;


    @PrePersist
    public void toCreate() {
        setCreateDate(LocalDateTime.now());
    }

    @PreUpdate
    public void toUpdate() {
        updateDate = LocalDateTime.now();
    }

}
