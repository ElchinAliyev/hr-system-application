package com.elchin.hrsystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "employees_performance")
public class EmployeePerformance extends AbstractEntity implements Serializable {
}
