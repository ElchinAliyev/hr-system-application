package com.elchin.hrsystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "interviews")
public class Interview extends AbstractEntity implements Serializable {

    private String name;
    private String surname;
    private String phone;
    private String mail;
    private LocalDate date;
}
