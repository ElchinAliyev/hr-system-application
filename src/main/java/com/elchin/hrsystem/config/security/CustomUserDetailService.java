package com.elchin.hrsystem.config.security;


import com.elchin.hrsystem.service.UserService;
import com.elchin.hrsystem.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userService.getUserByUsernameAndState(username, 1);
        if (optionalUser.isPresent()) {
            String name = optionalUser.get().getRole().getRoleName();
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_" + name);
            List<SimpleGrantedAuthority> simpleGrantedAuthorityList = new ArrayList<>();
            simpleGrantedAuthorityList.add(simpleGrantedAuthority);
            return new org.springframework.security.core.userdetails.User(username, optionalUser.get().getPassword(), simpleGrantedAuthorityList);
        }
        throw new UsernameNotFoundException("Username not found: " + username);
    }
}
