package com.elchin.hrsystem.config.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SuccessEventHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        User user = (User) authentication.getPrincipal();
        httpServletRequest.getSession().setAttribute("user", user);
        if (user.getAuthorities().iterator().next().getAuthority().equals("ROLE_HR")) {
            httpServletResponse.sendRedirect("/hr");
        } else if (user.getAuthorities().iterator().next().getAuthority().equals("ROLE_WORKER")) {
            httpServletResponse.sendRedirect("/worker");
        } else {
            throw new RuntimeException();
        }
    }
}
