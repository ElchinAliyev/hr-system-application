package com.elchin.hrsystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/worker")
public class WorkerController {

    @GetMapping
    public ModelAndView workerIndex() {
        ModelAndView modelAndView = new ModelAndView("worker");
        return modelAndView;
    }
}
