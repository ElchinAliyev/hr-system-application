package com.elchin.hrsystem.controller;

import com.elchin.hrsystem.dto.ApplicationFormDTO;
import com.elchin.hrsystem.model.ApplicationForm;
import com.elchin.hrsystem.model.Employee;
import com.elchin.hrsystem.model.Interview;
import com.elchin.hrsystem.model.Report;
import com.elchin.hrsystem.repository.projection.EmployeeDataProjection;
import com.elchin.hrsystem.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/hr")
public class HrController {

    private final ApplicationFormService applicationFormService;
    private final InterviewService interviewService;
    private final FileService fileService;
    private final EmployeeService employeeService;
    private final ReportService reportService;

    @GetMapping
    public ModelAndView hrIndex() {
        ModelAndView modelAndView = new ModelAndView("hr");
        List<ApplicationFormDTO> allApplicationForms = applicationFormService.getAllApplicationForms();
        modelAndView.addObject("allApplicationForms", allApplicationForms);
        List<EmployeeDataProjection> allEmployeeDataProjection = employeeService.getAllEmployeeDataProjection();
        modelAndView.addObject("allEmployeeDataProjection", allEmployeeDataProjection);
        List<Interview> allInterviews = interviewService.getAllInterviews();
        modelAndView.addObject("allInterviews", allInterviews);
        List<Report> allReports = reportService.getAllReports();
        modelAndView.addObject("allReports",allReports);
        return modelAndView;
    }


    @GetMapping("/resume/{id}")
    @ResponseBody
    public ResponseEntity<?> getResumeByFormId(@PathVariable int id) throws IOException {
        Resource resource = fileService.getCandidateResumeByCandidateFormId(id);
        String contentType = Files.probeContentType(Paths.get(resource.getFile().getPath()));
        return ResponseEntity.ok()
                .header("content-type", contentType)
                .body(resource);
    }

    @PostMapping("/interview")
    @ResponseBody
    public ResponseEntity<?> createNewInterview(@RequestParam(name = "name") String name,
                                                @RequestParam(name = "surname") String surname,
                                                @RequestParam(name = "mail") String mail,
                                                @RequestParam(name = "phone") String phone,
                                                @RequestParam(name = "intervyuDate") String intervyuDate) throws IOException {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(intervyuDate, dateTimeFormatter);

        Interview interview = new Interview();
        interview.setName(name);
        interview.setSurname(surname);
        interview.setMail(mail);
        interview.setPhone(phone);
        interview.setDate(localDate);

        boolean response = interviewService.insertNewInterview(interview);
        return ResponseEntity.ok(response);
    }


    @GetMapping("/interview")
    public ModelAndView getInterview() {
        ModelAndView modelAndView = new ModelAndView("interview");
        List<Interview> allInterviews = interviewService.getAllInterviews();
        modelAndView.addObject("allInterviews", allInterviews);
        return modelAndView;
    }
}
