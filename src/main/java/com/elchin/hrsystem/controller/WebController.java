package com.elchin.hrsystem.controller;

import com.elchin.hrsystem.dto.ApplicationFormDTO;
import com.elchin.hrsystem.model.ApplicationForm;
import com.elchin.hrsystem.model.Interview;
import com.elchin.hrsystem.model.Report;
import com.elchin.hrsystem.repository.projection.EmployeeDataProjection;
import com.elchin.hrsystem.service.ApplicationFormService;
import com.elchin.hrsystem.service.EmployeeService;
import com.elchin.hrsystem.service.InterviewService;
import com.elchin.hrsystem.service.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/")
public class WebController {

    private final ApplicationFormService applicationFormService;
    private final EmployeeService employeeService;
    private final InterviewService interviewService;
    private final ReportService reportService;

    @GetMapping
    public ModelAndView index() {
        List<EmployeeDataProjection> allEmployeeDataProjection = employeeService.getAllEmployeeDataProjection();
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("allEmployeeDataProjection", allEmployeeDataProjection);
        return modelAndView;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView("login");
        return modelAndView;
    }

    @GetMapping("/performance/query")
    public ModelAndView getPerformanceQuery() {
        ModelAndView modelAndView = new ModelAndView("performancequery");
        return modelAndView;
    }

    @PostMapping("/apply/job")
    public ModelAndView insertNewCandidateForm(@RequestParam(name = "name") String name,
                                               @RequestParam(name = "surname") String surname,
                                               @RequestParam(name = "mail") String mail,
                                               @RequestParam(name = "phone") String phone,
                                               @RequestParam(name = "position") String position,
                                               @RequestParam("file") MultipartFile file) throws IOException {

        ApplicationForm applicationForm = new ApplicationForm();
        applicationForm.setName(name);
        applicationForm.setSurname(surname);
        applicationForm.setMail(mail);
        applicationForm.setPhone(phone);
        applicationForm.setPositionName(position);

        applicationFormService.createNewApplicationForm(applicationForm, file);

        ModelAndView modelAndView = new ModelAndView("success");
        return modelAndView;
    }


    @GetMapping("/test")
    public ModelAndView test() {
        List<EmployeeDataProjection> allEmployeeDataProjection = employeeService.getAllEmployeeDataProjection();
        ModelAndView modelAndView = new ModelAndView("test");
        modelAndView.addObject("allEmployeeDataProjection", allEmployeeDataProjection);
        List<ApplicationFormDTO> allApplicationForms = applicationFormService.getAllApplicationForms();
        modelAndView.addObject("allApplicationForms", allApplicationForms);
        List<Interview> allInterviews = interviewService.getAllInterviews();
        modelAndView.addObject("allInterviews", allInterviews);
        return modelAndView;
    }

    @PostMapping("/report")
    public ModelAndView addNewReport(@RequestParam("type") String type,
                                     @RequestParam("text") String text) {
        ModelAndView modelAndView = new ModelAndView("success");
        Report report = new Report();
        report.setType(type);
        report.setText(text);

        reportService.addNewReport(report);
        return modelAndView;
    }

    @GetMapping("/success")
    public ModelAndView success() {
        ModelAndView modelAndView = new ModelAndView("success");
        return modelAndView;
    }
}
