package com.elchin.hrsystem.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ApplicationFormDTO implements Serializable {
    private int id;
    private String name;
    private String surname;
    private String mail;
    private String phone;
    private String positionName;
    private String created;
}
