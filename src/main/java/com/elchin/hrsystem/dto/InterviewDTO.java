package com.elchin.hrsystem.dto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class InterviewDTO {

    private String name;
    private String surname;
    private String phone;
    private String mail;
    private String date;
}
