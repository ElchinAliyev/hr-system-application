package com.elchin.hrsystem.service;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface FileService {

    String saveCandidateResume(MultipartFile file) throws IOException;

    Resource getCandidateResumeByCandidateFormId(int id) throws MalformedURLException;
}
