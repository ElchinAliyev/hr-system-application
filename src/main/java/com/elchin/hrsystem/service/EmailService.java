package com.elchin.hrsystem.service;

import com.elchin.hrsystem.model.Interview;

public interface EmailService {
    void notifyUserAboutInterview(Interview interview);
}
