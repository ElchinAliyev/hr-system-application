package com.elchin.hrsystem.service;

import com.elchin.hrsystem.model.User;

import java.util.Optional;

public interface UserService {

    Optional<User> getUserByUsernameAndState(String username, int state);
}
