package com.elchin.hrsystem.service;

import com.elchin.hrsystem.model.Report;

import java.util.List;

public interface ReportService {

    List<Report> getAllReports();

    void addNewReport(Report report);
}
