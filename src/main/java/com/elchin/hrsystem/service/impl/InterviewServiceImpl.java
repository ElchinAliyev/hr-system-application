package com.elchin.hrsystem.service.impl;

import com.elchin.hrsystem.model.Interview;
import com.elchin.hrsystem.repository.InterviewRepository;
import com.elchin.hrsystem.service.EmailService;
import com.elchin.hrsystem.service.InterviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InterviewServiceImpl implements InterviewService {

    private final InterviewRepository interviewRepository;
    private final EmailService emailService;

    @Override
    public boolean insertNewInterview(Interview interview) {
        interviewRepository.save(interview);
        emailService.notifyUserAboutInterview(interview);
        return true;
    }

    @Override
    public List<Interview> getAllInterviews() {
        return interviewRepository.findAll();
    }
}
