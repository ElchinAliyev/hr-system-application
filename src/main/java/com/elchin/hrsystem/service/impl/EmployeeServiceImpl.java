package com.elchin.hrsystem.service.impl;

import com.elchin.hrsystem.repository.EmployeeRepository;
import com.elchin.hrsystem.repository.projection.EmployeeDataProjection;
import com.elchin.hrsystem.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    public List<EmployeeDataProjection> getAllEmployeeDataProjection() {
        return employeeRepository.getAllEmployeeDataProjection();
    }
}
