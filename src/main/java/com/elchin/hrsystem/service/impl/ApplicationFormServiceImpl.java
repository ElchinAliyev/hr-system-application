package com.elchin.hrsystem.service.impl;

import com.elchin.hrsystem.dto.ApplicationFormDTO;
import com.elchin.hrsystem.model.ApplicationForm;
import com.elchin.hrsystem.repository.ApplicationFormRepository;
import com.elchin.hrsystem.service.ApplicationFormService;
import com.elchin.hrsystem.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApplicationFormServiceImpl implements ApplicationFormService {

    private final ApplicationFormRepository applicationFormRepository;
    private final FileService fileService;

    @Override
    public void createNewApplicationForm(ApplicationForm applicationForm, MultipartFile multipartFile) throws IOException {
        String resumePath = fileService.saveCandidateResume(multipartFile);
        applicationForm.setResumePath(resumePath);
        applicationFormRepository.save(applicationForm);
    }

    @Override
    public List<ApplicationFormDTO> getAllApplicationForms() {
        List<ApplicationFormDTO> applicationFormDTOList = new ArrayList<>();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
        List<ApplicationForm> applicationFormList = applicationFormRepository.findAll();
        applicationFormList.forEach(applicationForm -> {
            ApplicationFormDTO applicationFormDTO = new ApplicationFormDTO();

            applicationFormDTO.setId(applicationForm.getId());
            applicationFormDTO.setName(applicationForm.getName());
            applicationFormDTO.setSurname(applicationForm.getSurname());
            applicationFormDTO.setMail(applicationForm.getMail());
            applicationFormDTO.setPhone(applicationForm.getPhone());
            applicationFormDTO.setPositionName(applicationForm.getPositionName());
            applicationFormDTO.setCreated(applicationForm.getCreateDate().format(dateTimeFormatter));
            applicationFormDTOList.add(applicationFormDTO);
        });
        return applicationFormDTOList;
    }
}
