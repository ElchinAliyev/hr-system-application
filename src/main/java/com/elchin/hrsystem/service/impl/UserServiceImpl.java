package com.elchin.hrsystem.service.impl;

import com.elchin.hrsystem.service.UserService;
import com.elchin.hrsystem.model.User;
import com.elchin.hrsystem.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public Optional<User> getUserByUsernameAndState(String username, int state) {
        return userRepository.getUserByUsernameAndState(username, state);
    }
}
