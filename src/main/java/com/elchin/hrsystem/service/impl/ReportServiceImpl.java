package com.elchin.hrsystem.service.impl;

import com.elchin.hrsystem.model.Report;
import com.elchin.hrsystem.repository.ReportRepository;
import com.elchin.hrsystem.service.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ReportServiceImpl implements ReportService {

    private final ReportRepository reportRepository;

    @Override
    public List<Report> getAllReports() {
        return reportRepository.findAll();
    }

    @Override
    public void addNewReport(Report report) {
        reportRepository.save(report);
    }
}
