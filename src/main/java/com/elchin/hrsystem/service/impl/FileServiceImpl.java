package com.elchin.hrsystem.service.impl;

import com.elchin.hrsystem.model.ApplicationForm;
import com.elchin.hrsystem.repository.ApplicationFormRepository;
import com.elchin.hrsystem.service.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
public class FileServiceImpl implements FileService {

    private final String resumePath;
    private final ApplicationFormRepository applicationFormRepository;

    public FileServiceImpl(@Value("${resume.file.path}") String resumePath, ApplicationFormRepository applicationFormRepository) throws IOException {
        this.resumePath = resumePath;
        this.applicationFormRepository = applicationFormRepository;
        if (!Files.exists(Paths.get(resumePath))) {
            Files.createDirectory(Paths.get(resumePath));
        }
    }

    @Override
    public String saveCandidateResume(MultipartFile file) throws IOException {
        if (file != null) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddhhmmssSSS");
            String fileName = LocalDateTime.now().format(dateTimeFormatter);
            String path = resumePath + File.separator + fileName + getFileExtension(file.getOriginalFilename());
            try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(path)))) {
                bufferedOutputStream.write(file.getBytes());
            }
            return path;
        }
        return null;
    }

    @Override
    public Resource getCandidateResumeByCandidateFormId(int id) throws MalformedURLException {
        Optional<ApplicationForm> optionalApplicationForm = applicationFormRepository.getApplicationFormById(id);
        if (optionalApplicationForm.isPresent() && optionalApplicationForm.get().getResumePath() != null) {
            return new FileUrlResource(optionalApplicationForm.get().getResumePath());
        }
        return null;
    }

    public static String getFileExtension(String file) {
        String extension = "";
        if (file != null) {
            int pos = file.lastIndexOf(".");
            if (pos > 0) {
                extension = file.substring(pos);
            }
        }
        return extension;
    }

}
