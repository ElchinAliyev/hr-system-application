package com.elchin.hrsystem.service.impl;

import com.elchin.hrsystem.model.Interview;
import com.elchin.hrsystem.service.AsyncService;
import com.elchin.hrsystem.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {
    private final AsyncService asyncService;

    @Value("${spring.mail.username}")
    private String mail;

    @Override
    public void notifyUserAboutInterview(Interview interview) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(mail);
        simpleMailMessage.setTo(interview.getMail());
        simpleMailMessage.setSubject("Interview");
        simpleMailMessage.setText("Salam Hörmətli " + interview.getName() + " " + interview.getSurname() + ". Sizə " + interview.getDate() +
                " tarixində intervyu təyin olunmuşdur.");
        asyncService.sendEmail(simpleMailMessage);

    }
}
