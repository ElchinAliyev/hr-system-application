package com.elchin.hrsystem.service;

import com.elchin.hrsystem.model.Interview;

import java.util.List;

public interface InterviewService {
    boolean insertNewInterview(Interview interview);

    List<Interview> getAllInterviews();
}
