package com.elchin.hrsystem.service;

import com.elchin.hrsystem.dto.ApplicationFormDTO;
import com.elchin.hrsystem.model.ApplicationForm;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ApplicationFormService {

    void createNewApplicationForm(ApplicationForm applicationForm, MultipartFile multipartFile) throws IOException;

    List<ApplicationFormDTO> getAllApplicationForms();
}
