package com.elchin.hrsystem.service;

import com.elchin.hrsystem.repository.projection.EmployeeDataProjection;

import java.util.List;

public interface EmployeeService {

    List<EmployeeDataProjection> getAllEmployeeDataProjection();
}
